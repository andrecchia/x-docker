FROM ubuntu:jammy
ARG DOCKER_USER

# Install useradd (or similar) if the image doesn't provide it by default
# RUN (install command)

# Install some graphical app
RUN apt-get update && apt-get -y install gedit

RUN useradd $DOCKER_USER
USER $DOCKER_USER

# This sets the user's home owned by DOCKER_USER (owned by root otherwise)
WORKDIR /home/${DOCKER_USER}

CMD ["sleep", "infinity"]
